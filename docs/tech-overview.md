# Tech Overview #

This documents provides a high level overview of the technology being used by the US Social Forum. The document is intended for the technical support team to help participate in supporting the technology for the forum.

## Web Site ##

The web site is available via https://ussocialforum.net/. It is running Drupal 7.

## Database ##

The US Social Forum uses CiviCRM, which is enabled on the Drupal web site.

## Access to the web site ##

The Drupal web site has four roles:

 * Administrator: access to everything
 * editor: ability to change all content pages on the site
 * database: full access to the CiviCRM database
 * program: full access to the PMA and workshops

## Primary uses of the web site and database ##

The website and database provide the following key functions:

 * Registration for the US Social Forum. Anyone can register for the US Social Forum via this initial page link: https://www.ussocialforum.net/registration. They have two options: register as an individual or register as an organization. Each option is a CiviCRM Webform that records their payments and creates the registration record for the US Social Forum CiviCRM Event.
 * Registration for a PMA. There are several Peoples Movement Assembly sites, however, only one is collecting registration fees via our web site (Mexico). This registration form is available here: https://www.ussocialforum.net/aboutmexico. The PMA is a separate CiviCRM Event.
 * Tabling: Anyone can register to have a table at either San Jose or Philadelphia by filling out this webform: https://www.ussocialforum.net/table. When they fill it out, a donation of the type "Event Tabling" is created so we can keep track of them.
 * Workshop and PMA submissions: Workshop and PMA's are Drupal content types. They are not collected via CiviCRM (although a task is to make an activity record for each contact that has submitted one).

## Access to the code ##

The web site is hosted on the server keller.mayfirst.org. Access to the server is available via the username ussocialforum (e.g. ssh ussocialforum@keller.mayfirst.org).

The code is publicly available via https://gitlab.com/jamie/ussocialforum. Anyone who wants to help update the code can clone this repository and make merge requests.

The canonical git repository that the live site pulls from is on keller and accessible via ssh (gitosis@git.ussocialforum.net:ussocialforum.git). Therefore the change workflow is:

Coder:

 * Clone gitlab repository.
 * Make change and commit
 * Create merge request

Maintainer:

 * Review request
 * Push request to gitosis@keller.mayfirst.org
 * ssh into keller and pull

## Video Conferencing ##

We will be responsible for helping connect the different sites and the PMA locations using the jitsi web conference software available via: https://meet.mayfirst.org/. 
