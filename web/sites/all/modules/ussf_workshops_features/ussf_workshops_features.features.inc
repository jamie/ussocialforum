<?php
/**
 * @file
 * ussf_workshops_features.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ussf_workshops_features_node_info() {
  $items = array(
    'peoples_movement_assembly' => array(
      'name' => t('Peoples Movement Assembly'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('People\'s Movement Assemblies (PMAs) are 4 hour sessions that require 4 sponsoring organizations. Each sponsoring organization may submit a maximum of one PMA and one workshop.'),
    ),
    'workshop' => array(
      'name' => t('Workshop'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('A workshop is a 1.5 hour session. It requires 2 registered Sponsoring Organizations.  Each sponsoring organization may submit a maximum of one Workshop and one PMA.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
