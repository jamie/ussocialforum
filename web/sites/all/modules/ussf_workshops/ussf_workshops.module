<?php
/**
 * This module handles US Social Forum customizations
 * for the PMA and workshop submission process.
 **/

define('CIVICRM_CURRENT_EVENT_ID', 7);
define('CIVICRM_ORGANIZATION_ROLE', 5);

/**
 * Implements hook_form_alter() 
 */
function ussf_workshops_form_alter(&$form, &$form_state, $form_id) {
  $forms = array('peoples_movement_assembly_node_form', 'workshop_node_form');
  if(in_array($form_id, $forms)) {
    $path = drupal_get_path('module', 'ussf_workshops');
    // The js/css modifies the display of the Link type elements
    // so it can work for entering additional sponsoring organizations.
    drupal_add_js($path . '/ussf_workshops.js');
    drupal_add_css($path . '/ussf_workshops.css');
  }
  if(strpos($form_id, 'webform_client_form_') !== FALSE) {
    if($form['#action'] == '/register/organization') {
      $form['#validate'][] = 'ussf_workshops_validate_org_registration';
    }
  }
}

/**
 * Validate Organization registrations
 */
function ussf_workshops_validate_org_registration($form, &$form_state) {
  if(!array_key_exists('civicrm_1_contact_1_fieldset_fieldset', $form_state['values']['submitted'])) {
    // This is not page one.
    return;
  }
  $paid = $form_state['values']['submitted']['civicrm_1_contact_1_fieldset_fieldset']['civicrm_1_participant_1_participant_fee_amount'];
  // Make a copy for easier access
  $contacts = $form_state['values']['submitted'];

  // Unset the organization
  unset($contacts['civicrm_1_contact_1_fieldset_fieldset']);

  reset($contacts);
  $count = 0;
  while(list($k, $v) = each($contacts)) {
    if(!preg_match('/civicrm_[0-9][0-9]?_contact_1_fieldset_fieldset/', $k)) {
      // Not a contact.
      continue;
    }
    // if we have first name, last name or email ensure we have all fields
    // If first name, last name and email are all empty, then don't throw errors.
    $filled_in = FALSE;
    $field_no = 0;
    while(list($field, $value) = each($v)) {
      $field_no++;
      if($field_no > 3 && !$filled_in) {
        // If we are on field 4 and none of the fields are filled in
        // then we assume the block is empty and don't need to validate.
        // Also, don't count this as a contact.
        $count--;
        break;
      }
      if($field_no < 4) {
        // If we are still on the first three fields, check for a value
        // and if we have a value, note it.
        if(!empty($value)) {
          $filled_in = TRUE;
        }
      }
      if($filled_in) {
        if(empty($value)) {
          form_set_error($field, t('You must complete all fields for all registrants.'));
          break;
        }
      }
    }
    $count++;
  } 

  // These are the allowed payment amounts (keys) and the max number of
  // participants allowed (value) as provided by Shamako.
  $pay_options = array(
    '125' => 3,
    '200' => 10,
    '380' => 20,
    '560' => 30,
    '740' => 40,
    '920' => 50,
    '1110' => 60,
    '1280' => 80,
    '1350' => 100,
  );
  if(!array_key_exists($paid, $pay_options)) {
    form_set_error('civicrm_2_participant_1_participant_fee_amount', t("You seem to have chosen a fee that is not being offered. Please try again."));
  }
  $max = $pay_options[$paid];

  if($count > $max) {
      form_set_error('civicrm_2_participant_1_participant_fee_amount', t("You are trying to register more people than is allowed for your fee level."));
  }
}

/**
 * Implements hook_node_validate().
 */
function ussf_workshops_node_validate($node, $form, &$form_state) {
  if($node->type == 'peoples_movement_assembly' || $node->type == 'workshop') {
    // Additional validation for USSF submissions.
    ussf_workshops_word_count($form, 'field_short_description', 25);
    ussf_workshops_word_count($form, 'field_description', 250);
    ussf_workshops_word_count($form, 'field_submission_contact_org_des', 50);
    // Basic web and email validation
    $url = ussf_workshops_get_field_value($form, 'field_submission_contact_org_web');
    if(!preg_match('/^http/', $url)) {
      form_set_error('field_submission_contact_org_web', t("The organization web site (@url) should start with http", array('@url' => $url)));
    }
    $email = ussf_workshops_get_field_value($form, 'field_submission_contact_email');
    if(!preg_match('/@[a-zA-Z\-0-9]+\./', $email)) {
      form_set_error('field_submission_contact_email', t("Contact email ($email) doesn't look like a valid email address."));
    }
    $site = ussf_workshops_get_field_value($form, 'field_site');
    ussf_workshops_validate_email_and_site($email, $site);
  }
}

function ussf_workshops_word_count($form, $field, $limit) {
  // Get the value of the field being submitted
  $value = ussf_workshops_get_field_value($form, $field); 
  $words = explode(' ', $value);
  if(count($words) > $limit) {
    $title = $form[$field][$lang][0]['value']['#title'];
    $args = array('@title' => $title, '@limit' => $limit);
    form_set_error($field, t("Please limit @title to @limit words.", $args));
  }
}

/**
 * Figure out how to extract the value of the given $field from the given
 * form array or object.
 */
function ussf_workshops_get_field_value($form, $field) {
  // Force form to array so we can also use this function for objects.
  if(is_array($form)) {
    $value_field = '#value';
  }
  else {
    $form = (array)$form;
    $value_field = 'value';
  }

  // See if the language is encoded in the form/object.
  $lang = 'und';
  if(is_array($form['language']) && array_key_exists($value_field, $form['language'])) {
    $lang = $form['language'][$value_field];
  }
  elseif(is_string($form['language'])) {
    $lang = $form['language'];
  }

  // If the encoded language is not there, fall back to und.
  if(!array_key_exists($lang, $form[$field])) {
    $lang = 'und';
    if(!array_key_exists($lang, $form[$field])) {
      drupal_set_message("Error parsing the right language.", 'error');
      return FALSE;
    }
  }
  if(array_key_exists(0, $form[$field][$lang])) {
    if(array_key_exists('value', $form[$field][$lang][0])) {
      if(is_string($form[$field][$lang][0]['value'])) {
        return $form[$field][$lang][0]['value'];
      } 
      else {
        return $form[$field][$lang][0]['value'][$value_field];
      }
    }
    elseif(array_key_exists($value_field, $form[$field][$lang][0])) {
      return $form[$field][$lang][0][$value_field];
    }
    else {
      drupal_set_message(t("Failed to find value for @field.", array('@field' => $field)));
    }
  }
  else {
    return $form[$field][$lang][$value_field];
  }

}

/**
 * Ensure the email address is registered via an organizational
 * registration.
 **/
function ussf_workshops_validate_email_and_site($email, $site) {
  civicrm_initialize();
  try {
    $results = civicrm_api3('Contact', 'get', array('email' => $email, 'return' => 'id'));
  }
  catch (CiviCRM_API3_Exception $e) {
    $err = t("There was an error retrieving your registraiton information. Please try again later.");
    form_set_error('field_submission_contact_email', $err);
    return;
  }
  $error_args = array('@email' => $email);
  // First check: are they registered at all?
  if(!empty($results['values'])) {
    $ids = array_keys($results['values']);
    // We could have duplicate records with the same email address. If so, check them all
    $ids_clause = "(" . implode(',', $ids) . ")";
    $sql = "SELECT contact_id, contact_type FROM civicrm_participant p JOIN 
      civicrm_participant_status_type st ON p.status_id = st.id JOIN civicrm_contact c 
      ON c.id = p.contact_id WHERE contact_id IN $ids_clause AND st.name = 'Registered' 
      AND event_id = %0";
    $params = array(0 => array(CIVICRM_CURRENT_EVENT_ID, 'Integer'));
    $dao = CRM_Core_DAO::executeQuery($sql, $params);
    if($dao->N == 0) {
      // Nope, not registered.
      $err = t("The email address @email is not registered for the forum. You must first register as an organization before you can submit a workshop or PMA", $error_args);
      form_set_error('field_submission_contact_email', $err);
      return;
    }
    while($dao->fetch()) {
      // This means they are registered. 
      // If the contact type is organization, then it means they are registered as an organization
      // and that is good enough.
      if($dao->contact_type == 'Organization') {
        return TRUE;
      }
      
      // If not, see if they are related to a registered organization. The event participant role_id
      // CIVICRM_ORGANIZATION_ROLE is specifically designed for organizations with the special
      // registration status ("organization").
      $contact_ids_clause = 
      $sql = "SELECT p.contact_id FROM civicrm_participant p JOIN civicrm_relationship WHERE role_id = %0 
        AND (( contact_id_a = %1 AND contact_id_b = p.contact_id) OR ( contact_id_a = p.contact_id AND 
        contact_id_b = %1) )";
      $params = array(
        0 => array(CIVICRM_ORGANIZATION_ROLE, 'Integer'),
        1 => array($dao->contact_id, 'Integer'),
      );
      $dao = CRM_Core_DAO::executeQuery($sql, $params);
      if($dao->N > 0) {
        return TRUE;
      }
      else {
        $err = t("The email address @email is registered for the fourm, however, it is registered as an individual. You must be registered as an organization to submit a workshop or PMA.", $error_args);
      }
    }
  }
  else {
    // This actually means their email address is not in our database at all.
    $err = t("The email address @email is not registered for the forum. You must first register as an organization before you can submit a workshop or PMA", $error_args);
    form_set_error('field_submission_contact_email', $err);
    return;
  }
  form_set_error('field_submission_contact_email', $err);
}

/**
 * Impelentation of hook_action_info
 */
function ussf_workshops_action_info() {
  return array(
    'ussf_workshops_notify_submitter_action' => array(
      'type' => 'node',
      'label' => t('Email Workshop or PMA submitters after form is submitted'),
      'triggers' => array('node_insert'),
      'configurable' => FALSE
    )
  );
}

/**
 * Impelementation of notify_submitter action.
 */
function ussf_workshops_notify_submitter_action($object, $action_ids, $context = NULL, $a1 = NULL, $a2 = NULL) {
  if($object->type != 'workshop' && $object->type != 'peoples_movement_assembly') {
    return;
  }
  $email_address = ussf_workshops_get_field_value($object, 'field_submission_contact_email');
  $params = array(
    'type' => $object->type,
    'title' => $object->title,
    'site' => ussf_workshops_get_field_value($object, 'field_site'),
    'first_name' => ussf_workshops_get_field_value($object, 'field_submission_contact_fname')
  );
  drupal_mail('ussf_workshops', 'notify_submitter', $email_address, language_default(), $params);

}

/**
 * Implementation of hook_mail (see ussf_workshops_notify_submitter_action)
 */
function ussf_workshops_mail($key, &$message, $params) {
  $submission = NULL;
  if($params['type'] == 'workshop') {
    $submission = 'workshop';
  }
  elseif($params['type'] == 'peoples_movement_assembly') {
    $submission = "Peoples Movement Assembly";
  }
  else {
    drupal_set_message(t("Not sending message on submission of @type.", array('@type' => $params['type'])));
    return;
  }
  
  $location = NULL;
  if($params['site'] == 'Philadelphia, PA') {
    $location = "Philadelphia, PA, June 25-28";
  }
  elseif($params['site'] == 'San Jose, CA') {
    $location = "San Jose, CA, June 24-28";
  }
  elseif($params['site'] == 'Jackson, MS') {
    $location = "[Undecided]";
  }
  else {
    drupal_set_message(t("Not sending email: unknown site (@site).", array('@site' => $params['site'])));
    return;
  }
  switch($key) {
    case 'notify_submitter':
      $message['subject'] = t("Your US Social Forum @submission has been accepted", array('@submission' => $submission));
      $message['body'][] = 
        t("Hello @firstname, 
We are pleased to inform you that your @submission, @title, has been accepted for the 2015 US Social Forum in @location. 

We ask that you bring whatever materials are needed for your workshop – butcher block paper, markers, tape, electronic equipment (computers, projection, streaming, etc.). If you know participants will be multilingual, please arrange for interpretation at your workshop.

We will be releasing additional information on venues, lodging and programming over the next couple of weeks, so stay tuned and continue to check out the website for regular updates at https://www.ussocialforum.net and email us at info@ussocialforum.net with any questions.

Again, we are very excited to include your workshop in the 2015 US Social Forum. See you there!

Another World is Possible, Another System is Necessary!

The National Planning Committee, Philly LOC, SJBAY LOC and the United States Social Forum
         
Shamako Noble, National Coordinator
info@ussocialforum.net 
408-624-2999 

https://www.ussocialforum.net 
http://www.ussfphilly.org 
http://www.ussfsanjose.com  
", 
          array(
            '@title' => $params['title'],
            '@firstname' => $params['first_name'],
            '@location' => $location,
            '@submission' => $submission
          )
        );



        
      break;

  }

}
