// Update labels for Link fields so it is more sensible.
jQuery( document ).ready(function() {
  jQuery("label[for='edit-field-additional-sponsors-und-0-title']").html("Organization");
  jQuery("label[for='edit-field-additional-sponsors-und-1-title']").html("Organization");
  jQuery("label[for='edit-field-additional-sponsors-und-2-title']").html("Organization");

  jQuery("label[for='edit-field-additional-sponsors-und-0-url']").html("Web site address");
  jQuery("label[for='edit-field-additional-sponsors-und-1-url']").html("Web site address");
  jQuery("label[for='edit-field-additional-sponsors-und-2-url']").html("Web site address");

});

