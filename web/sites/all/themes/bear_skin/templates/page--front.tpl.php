<div id="page">
  <div id="wrapper" class="wrapper front-header">
    <header id="front-header" role="banner">

      <?php if ($site_name || $site_slogan): ?>
        <hgroup id="front-name-and-slogan">
          <?php if ($site_name): ?>
            <h1 id="front-site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <h2 id="front-site-slogan"><?php print $site_slogan; ?></h2>
          <?php endif; ?>
            
            <a id="pagetop" href="#navigation">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                <circle cx="50" cy="50" r="36.5" class="down-arrow"/>
              <g>
                <g>
                  <line x1="26.8" y1="46.8" x2="51" y2="71.2" class="down-arrow"/>
                  <line x1="73.2" y1="46.8" x2="50" y2="70" class="down-arrow"/>
                </g>
                <g>
                  <line x1="26.8" y1="36.8" x2="51" y2="61.2" class="down-arrow"/>
                  <line x1="73.2" y1="36.8" x2="50" y2="60" class="down-arrow"/>
                </g>
              </g>
              </svg>
            </a>
        
        </hgroup><!-- /#name-and-slogan -->
      <?php endif; ?>

      <?php print render($page['header']); ?>

    </header>
  </div>

      <div class="wrapper navigation">
        <div id="navigation" tabindex="-1">
          <?php print render($page['navigation']); ?>
        </div><!-- /#navigation -->
      </div>

  <div class="wrapper main">
    <div id="main">
      <div id="content" class="column" role="main">
        <?php print render($page['highlighted']); ?>
        <?php print $breadcrumb; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
      </div><!-- /#content -->

      <?php
        // Render the sidebars to see if there's anything in them.
        $sidebar_first  = render($page['sidebar_first']);
        $sidebar_second = render($page['sidebar_second']);
      ?>

      <?php if ($sidebar_first || $sidebar_second): ?>
        <aside class="sidebars">
          <?php print $sidebar_first; ?>
          <?php print $sidebar_second; ?>
        </aside><!-- /.sidebars -->
      <?php endif; ?>

    </div><!-- /#main -->
  </div>
</div><!-- /#page -->
<div class="wrapper footer">
  <?php print render($page['footer']); ?>
</div>
