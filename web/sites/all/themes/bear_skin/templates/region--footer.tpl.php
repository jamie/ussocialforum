<?php
/**
 * @file
 * Returns the HTML for the footer region.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728140
 */
?>
<?php if ($content): ?>
  <footer id="footer" class="<?php print $classes; ?>">
	  <?php print $content; ?>
	   <div class="footer-links-wrapper">
	    <a href=" https://www.facebook.com/ussocialforum?fref=ts"><h3>Follow us on Facebook!</h3></a>
	  	<a href="https://twitter.com/ussf"><h3>Follow us on Twitter!</h3></a>
  	<div>
  </footer>
<?php endif; ?>