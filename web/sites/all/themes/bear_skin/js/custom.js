(function ($) {
//the code below enables smooth scrolling to work
  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });

//the code below enables the sticky nav-bar
  $(function() {
  	$(window).on('scroll',function() {
    	var scrolltop = $(this).scrollTop();

    	var $nav = $("div.navigation");
    	if (scrolltop >= $nav.offset().top) {
    		$('#navigation').css('top', '0');
      	$('#navigation').css('position', 'fixed');
      	$('#navigation').css('z-index', '999');    	
    	
    	}
    	else {
    		$('#navigation').css('position', 'relative');
    	}
  	});
  });

}(jQuery));

